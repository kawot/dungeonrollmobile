﻿using UnityEngine;
using System.Collections;

public static class GameUtils
{
    public static void Init()
    {
        cam = Camera.main;
    }

    public static Camera cam;

    public static Vector3 MouseOnGround(float height = 0f)
    {
        var ray = cam.ScreenPointToRay(Input.mousePosition);
        var plane = new Plane(Vector3.up, Vector3.up * height);
        float distance;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

}
