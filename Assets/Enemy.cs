﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : Figure
{
    public static string[] Type = new string[]
    {
        "Potion",
        "Chest",
        "Goblin",
        "Undead",
        "Ooze",
        "Dragon"
    };

    public static List<Enemy> left = new List<Enemy>();

    public void Remove()
    {
        Destroy(gameObject);
    }

    private void Start()
    {
        left.Add(this);
    }

    

}
