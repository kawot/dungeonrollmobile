﻿using UnityEngine;
using System.Collections;

[System.Serializable]
[CreateAssetMenu(fileName = "GameConfig", menuName = "Settings/Main", order = 0)]
public class GameSettings : ScriptableObject
{
    public int whiteDiceCount  = 7;
    public int blackDiceCount  = 7;
    public float diceDelay = .2f;
    public GameObject whiteDicePrefab;
    public GameObject blackDicePrefab;

    public GameObject[] members;
    public GameObject[] enemies;
    
}