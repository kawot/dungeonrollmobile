﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool
{

    private GameObject defaultItem;
    private List<GameObject> items = new List<GameObject>();
    private bool isRestricted;

    public Pool(GameObject defaultItem, int premadeCount = 0, bool isRestricted = true)
    {
        this.defaultItem = defaultItem;
        for (var i = 0; i < premadeCount; i++)
        {
            Create();
        }
        this.isRestricted = isRestricted;
    }

    public GameObject Pull()
    {
        if (items.Count == 0)
        {
            if (isRestricted)
            {
                Debug.Log("no more dices available");
                return null;
            }
            else Create();
        }
        var item = items[0];
        items.Remove(item);
        item.SetActive(true);
        return item;
    }

    public void Put(GameObject item)
    {
        item.SetActive(false);
        items.Add(item);
    }

    private void Create()
    {
        var newItem = Object.Instantiate(defaultItem);
        newItem.SetActive(false);
        items.Add(newItem);
    }
}
