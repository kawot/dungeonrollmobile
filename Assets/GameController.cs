﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public static int reservedDiceCount = 0;

    [SerializeField] private GameSettings gameSettings;
    [SerializeField] private Transform whiteDiceSpawnPoint;
    [SerializeField] private Transform blackDiceSpawnPoint;

    private Pool whiteDicePool;
    private Pool blackDicePool;
    private List<Dice> whiteDices = new List<Dice>();
    private List<Dice> blackDices = new List<Dice>();
    private int level = 0;

    // Use this for initialization
    void Start()
    {
        instance = this;
        GameUtils.Init();

        whiteDicePool = new Pool(gameSettings.whiteDicePrefab, gameSettings.whiteDiceCount, false);
        blackDicePool = new Pool(gameSettings.blackDicePrefab, gameSettings.blackDiceCount, true);

        StartCoroutine(AddDices(gameSettings.whiteDiceCount, whiteDiceSpawnPoint.position, whiteDices));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) ReadDices();
        if (Input.GetKeyDown(KeyCode.N)) NextLevel();
    }

    private void NextLevel()
    {
        level++;
        var count = level;
        if (count + reservedDiceCount > gameSettings.blackDiceCount) count = gameSettings.blackDiceCount - reservedDiceCount;
        reservedDiceCount += count;
        StartCoroutine(AddDices(count, blackDiceSpawnPoint.position, blackDices));
    }

    private void ReadDices()
    {
        MorphDices(whiteDices, gameSettings.members, whiteDicePool);
        MorphDices(blackDices, gameSettings.enemies, blackDicePool);
    }

    private void MorphDices(List<Dice> dices, GameObject[] figures, Pool dicePool)
    {
        for (var i = 0; i < dices.Count; i++)
        {
            var pos = dices[i].transform.position;
            pos.y = 0;
            var side = dices[i].GetPhysicSide() - 1;
            if (side > 5) side = 5; //REMOVE THIS WHEN DICE LOGIC IS CHANDED
            var newFigure = Instantiate(figures[side], pos, Quaternion.identity);
            newFigure.GetComponent<Figure>().Init(side);
            dicePool.Put(dices[i].gameObject);
        }
        dices.Clear();
    }

    IEnumerator AddDices(int count, Vector3 pos, List<Dice> dices)
    {
        for (var i = 0; i < count; i++)
        {
            var newDice = whiteDicePool.Pull();
            if (newDice == null)
            {

            }
            else
            {
                newDice.transform.position = pos + Random.insideUnitSphere;
                newDice.transform.rotation = Random.rotation;
                dices.Add(newDice.GetComponent<Dice>());
            }
            yield return new WaitForSeconds(gameSettings.diceDelay);
        }

        //TEMP PART
        yield return new WaitForSeconds(1f);
        ReadDices();
    }

    
}
