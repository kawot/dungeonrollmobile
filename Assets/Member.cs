﻿using UnityEngine;
using System.Collections;

public class Member : Figure
{
    public static string[] Role = new string[]
    {
        "Champion",
        "Rogue",
        "Fighter",
        "Cleric",
        "Wizard",
        "Scroll"
    };

    private Enemy enemy;

    private void OnMouseDrag()
    {
        transform.position = GameUtils.MouseOnGround(.25f);
    }

    //private void OnMouseEnter()
    //{
    //    Debug.Log("Enter");
    //}

    //private void OnMouseDown()
    //{
    //    Debug.Log("Click");
    //}

    private void OnMouseUp()
    {
        if (enemy == null)
        {
            transform.position = startPos;
            Debug.Log("Miss");
        }
        else
        {
            Debug.Log(Role[side] + " - " + Enemy.Type[enemy.side]);
            if (side == enemy.side || side == 0 || enemy.side == 0)
            {
                for (var i = Enemy.left.Count - 1; i >= 0; i--)
                {
                    var e = Enemy.left[i];
                    if (e.side == enemy.side)
                    {
                        e.Remove();
                        Enemy.left.RemoveAt(i);
                        GameController.reservedDiceCount--;
                    }
                }
            }
            else
            {
                enemy.Remove();
                Enemy.left.Remove(enemy);
                GameController.reservedDiceCount--;
            }
            enemy = null;
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (enemy != null) return;

        var newEnemy = other.GetComponent<Enemy>();
        if (newEnemy == null) Debug.LogError("Starnge " + other.name);
        enemy = newEnemy;
    }

    private void OnTriggerExit(Collider other)
    {
        enemy = null;
    }
}
