﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{
    [SerializeField] float rollForce = 100f;
    [SerializeField] float reRollForce = 100f;

    private Rigidbody body;

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q)) Roll();
    }

    private void Roll()
    {
        body.AddForceAtPosition((Vector3.up + Random.insideUnitSphere).normalized * rollForce, Random.insideUnitSphere * transform.localScale.x + transform.position);
    }

    public int GetPhysicSide()
    {
        var subVector = transform.up * 2.5f + transform.right * 1.5f + transform.forward * 0.5f;
        return Mathf.RoundToInt(subVector.y - transform.position.y + 4f);
    } 
}
